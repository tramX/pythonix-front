import { Directive, ElementRef, Attribute, Input, OnInit } from "@angular/core";

@Directive({
  selector: "[add-fa-class-notification-directive]",
})
export class AddClassNotification implements OnInit {
  
  @Input() notificationType;
  
  constructor(private element: ElementRef ) {
  }
  
  ngOnInit(){
    this.element.nativeElement.classList.add(this.notificationType);
  }
  
}
