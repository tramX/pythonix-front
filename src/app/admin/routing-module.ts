import { ClientComponent } from './client/client.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let routing = RouterModule.forChild([
  { path: ":id", component: ClientComponent },
  { path: "**", redirectTo: "" }
]);

@NgModule({
  imports: [
    routing
  ],
  exports: []
})
export class AdminRoutingModule { }
