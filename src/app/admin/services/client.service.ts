import { iEntityGroupClient } from '../../../_common/ClientEntity';
import { Injectable } from '@angular/core';

@Injectable()
export class ClientService {

  constructor() { }
  
  public groupsClients: Array<iEntityGroupClient> = [
    {id: 1, title: 'Кабель'},
    {id: 2, title: 'Радио'}
  ];
}
