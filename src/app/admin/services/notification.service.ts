import { iEntityNotification, FaClassesNotification } from '../../../_common/NotificationEntity';
import { Injectable } from '@angular/core';

@Injectable()
export class NotificationService {

  constructor() { }
  
  notifications: Array<iEntityNotification> = [
    { title: 'New Comment', dateTime: '4 minutes ago', faClasses: FaClassesNotification['comment']},
    { title: 'Message Sent', dateTime: '4 minutes ago', faClasses: FaClassesNotification['message']},
    { title: 'New Task', dateTime: '4 minutes ago', faClasses: FaClassesNotification['tasks']},
    { title: 'Server Rebooted', dateTime: '4 minutes ago', faClasses: FaClassesNotification['upload']},
    { title: 'Server Crashed!', dateTime: '4 minutes ago', faClasses: FaClassesNotification['crashed']},
    { title: 'Server Not Responding', dateTime: '4 minutes ago', faClasses: FaClassesNotification['warning']},
    { title: 'New Order Placed', dateTime: '4 minutes ago', faClasses: FaClassesNotification['order']},
    { title: 'Payment Received', dateTime: '4 minutes ago', faClasses: FaClassesNotification['pay']}
  ];

}
