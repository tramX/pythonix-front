import { AppRoutingModule } from '../app-routing.module';
import { AddClassNotification } from './add-fa-class-notification-directive';
import { ClientService } from './services/client.service';
import { NotificationService } from './services/notification.service';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClientComponent } from './client/client.component';
import { RouterModule } from '@angular/router';

let routing = RouterModule.forChild([
  { path: ":id", component: ClientComponent },
  { path: "**", redirectTo: "" }
]);


@NgModule({
  declarations: [AddClassNotification, ClientComponent],
  imports: [ routing, CommonModule],
  exports: [AddClassNotification],
  providers: [NotificationService, ClientService],
})
export class AdminModule { }

