import { IndexPageComponent } from './index-page/index-page.component';
import {RouterModule, Routes} from "@angular/router";
import { NgModule } from '@angular/core';


const routes: Routes = [
  {path: '', component: IndexPageComponent, pathMatch: 'full'},
  { path: "clients", loadChildren: "app/admin/admin.module#AdminModule"}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {
}