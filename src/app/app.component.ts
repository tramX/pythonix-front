import { ClientService } from './admin/services/client.service';
import { NotificationService } from './admin/services/notification.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public notifications;
  public groupsClients;
  
  constructor(private notificationService: NotificationService, private clientService: ClientService) { 
    this.notifications = notificationService.notifications;
    this.groupsClients = clientService.groupsClients;
  }
}
