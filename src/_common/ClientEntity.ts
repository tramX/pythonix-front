export interface iEntityGroupClient {
  id: number,
  title: string
}


export interface iEntityClient {
  id: number,
  clientGroup: string,
  login: string,
  ip: string,
  balance: number,
  internetStatus: boolean;
}