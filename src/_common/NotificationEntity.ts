export interface iEntityNotification {
  title: string,
  dateTime: string,
  faClasses: string
}


export const FaClassesNotification = {
  "comment": "fa-comment",
  "message": "fa-envelope",
  "tasks": "fa-tasks",
  "upload": "fa-upload",
  "crashed": "fa-bolt",
  "warning": "fa-warning",
  "order": "fa-shopping-cart",
  "pay": "fa-money"
}









